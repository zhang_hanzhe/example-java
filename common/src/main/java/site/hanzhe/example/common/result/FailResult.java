package site.hanzhe.example.common.result;

/**
 * Success
 *
 * @author 张涵哲
 * @since 2024-08-08 13:02:02
 */
public class FailResult<T> extends Result<T> {

    private final static int code = 500;
    private final static String msg = "fail";

    public FailResult() {
        super(code, msg, null);
    }
    public FailResult(String msg) {
        super(code, msg, null);
    }

}
