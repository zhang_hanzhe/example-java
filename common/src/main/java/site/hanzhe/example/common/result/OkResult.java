package site.hanzhe.example.common.result;

/**
 * Success
 *
 * @author 张涵哲
 * @since 2024-08-08 13:02:02
 */
public class OkResult<T> extends Result<T> {

    private final static int code = 200;
    private final static String msg = "ok";

    public OkResult() {
        super(code, msg, null);
    }
    public OkResult(String msg) {
        super(code, msg, null);
    }
    public OkResult(T data) {
        super(code, msg, data);
    }

}
