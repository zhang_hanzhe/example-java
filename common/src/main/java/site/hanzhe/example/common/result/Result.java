package site.hanzhe.example.common.result;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * 通用返回结果类
 *
 * @author 张涵哲
 * @since 2024-08-08 12:38:59
 */
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Result<T> {

    private int code;
    private String msg;
    private T data;

}
