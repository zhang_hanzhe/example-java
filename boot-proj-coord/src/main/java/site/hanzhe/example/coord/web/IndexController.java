package site.hanzhe.example.coord.web;

import cn.hutool.core.map.MapUtil;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import site.hanzhe.example.coord.dto.RotateProjDto;
import site.hanzhe.example.coord.service.IndexService;

import javax.annotation.Resource;
import java.util.Map;

/**
 * IndexController
 *
 * @author 张涵哲
 * @since 2023-06-24 21:04
 */
@RestController
public class IndexController {

    @Resource
    private IndexService service;

    @PostMapping("/rotateProj")
    public Object rotateProj(@RequestBody RotateProjDto dto) {
        return success(service.rotateProj(dto));
    }

    /**
     * 请求成功返回结果
     *
     * @author 张涵哲
     * @since 2023-06-24 22:18
     */
    private Map<String, Object> success(Object data) {
        Map<String, Object> result = MapUtil.newHashMap();
        result.put("code", 200);
        result.put("msd", "success");
        result.put("data", data);
        return result;
    }

}
