package site.hanzhe.example.coord.entity;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjUtil;
import lombok.NoArgsConstructor;

import java.util.ArrayList;

/**
 * 继承了List集合的3D坐标对象，顺序依次为 x y z
 * @author 张涵哲
 * @since 2023-06-24 21:24
 */
@NoArgsConstructor
public class Point3D extends ArrayList<Double> {

    public Point3D(double x, double y, double z) {
        this.add(x);
        this.add(y);
        this.add(z);
    }

    public double getX() {
        return ObjUtil.defaultIfNull(CollUtil.get(this, 0), 0.0);
    }
    public void setX(double x) {
        this.set(0, x);
    }

    public double getY() {
        return ObjUtil.defaultIfNull(CollUtil.get(this, 1), 0.0);
    }
    public void setY(double y) {
        this.set(1, y);
    }

    public double getZ() {
        return ObjUtil.defaultIfNull(CollUtil.get(this, 2), 0.0);
    }
    public void setZ(double z) {
        this.set(2, z);
    }

}