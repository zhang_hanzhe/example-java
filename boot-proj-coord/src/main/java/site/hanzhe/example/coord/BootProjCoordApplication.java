package site.hanzhe.example.coord;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * BootProjCoordApplication
 *
 * @author 张涵哲
 * @since 2023-06-24 21:00
 */
@SpringBootApplication
public class BootProjCoordApplication {

    public static void main(String[] args) {
        SpringApplication.run(BootProjCoordApplication.class, args);
    }

}
