package site.hanzhe.example.coord.service;

import cn.hutool.core.collection.CollUtil;
import org.springframework.stereotype.Service;
import site.hanzhe.example.coord.dto.RotateProjDto;
import site.hanzhe.example.coord.entity.Point2D;
import site.hanzhe.example.coord.utils.PointUtil;

import java.util.List;
import java.util.Optional;

/**
 * IndexService
 *
 * @author 张涵哲
 * @since 2023-06-24 22:15
 */
@Service
public class IndexService {

    public List<Point2D> rotateProj(RotateProjDto dto) {
        // 将多维数组解析为3D坐标组
        return Optional.of(dto.getPoints())
                // 将3D坐标组进行旋转
                .map(points3D -> PointUtil.rotate(points3D, dto.getRotateX(), dto.getRotateY(), dto.getRotateZ()))
                // 将旋转后的3D坐标轴以正交投影方法转为2D坐标组
                .map(PointUtil::toPointList2D)
                // 将2D坐标组的坐标值四舍五入，保留两位小数
                .map(point2DS -> PointUtil.round(point2DS, 2))
                // 非空校验 返回结果
                .orElse(CollUtil.newArrayList());
    }

}
