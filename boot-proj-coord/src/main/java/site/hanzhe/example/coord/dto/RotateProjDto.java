package site.hanzhe.example.coord.dto;

import lombok.Data;
import site.hanzhe.example.coord.entity.Point3D;

import java.util.List;

@Data
public class RotateProjDto {

    /**
     * 三维坐标点列表
     */
    private List<Point3D> points;

    /**
     * X轴旋转角度
     */
    private double rotateX;

    /**
     * Y轴旋转角度
     */
    private double rotateY;

    /**
     * Z轴旋转角度
     */
    private double rotateZ;

}