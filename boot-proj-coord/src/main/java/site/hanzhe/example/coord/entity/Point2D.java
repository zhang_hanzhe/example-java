package site.hanzhe.example.coord.entity;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjUtil;
import lombok.NoArgsConstructor;

import java.util.ArrayList;

@NoArgsConstructor
public class Point2D extends ArrayList<Double> {

    public Point2D(double x, double y) {
        this.add(x);
        this.add(y);
    }

    public double getX() {
        return ObjUtil.defaultIfNull(CollUtil.get(this, 0), 0.0);
    }
    public void setX(double x) {
        this.set(0, x);
    }

    public double getY() {
        return ObjUtil.defaultIfNull(CollUtil.get(this, 1), 0.0);
    }
    public void setY(double y) {
        this.set(1, y);
    }

}