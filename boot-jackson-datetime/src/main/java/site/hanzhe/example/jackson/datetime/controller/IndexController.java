package site.hanzhe.example.jackson.datetime.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import site.hanzhe.example.common.result.OkResult;
import site.hanzhe.example.common.result.Result;
import site.hanzhe.example.jackson.datetime.param.RequestParam;
import site.hanzhe.example.jackson.datetime.vo.ResponseVO;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.YearMonth;
import java.util.Date;

/**
 * IndexController
 *
 * @author 张涵哲
 * @since 2024-08-08 12:34:01
 */
@RestController
public class IndexController {

    @GetMapping("/response")
    public Result<Object> response() {
        ResponseVO vo = new ResponseVO();
        vo.setYearMonth(YearMonth.now());
        vo.setLocalDate(LocalDate.now());
        vo.setLocalTime(LocalTime.now());
        vo.setLocalDateTime(LocalDateTime.now());
        vo.setDate(new Date());
        return new OkResult<>(vo);
    }

    @PostMapping("/request")
    public Result<Object> request(@RequestBody RequestParam param) {
        System.out.println(param);
        return new OkResult<>();
    }

}
