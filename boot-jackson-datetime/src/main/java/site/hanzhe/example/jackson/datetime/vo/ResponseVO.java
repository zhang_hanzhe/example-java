package site.hanzhe.example.jackson.datetime.vo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.*;
import java.util.Date;

/**
 * ResponseVO
 *
 * @author 张涵哲
 * @since 2024-08-08 13:07:34
 */
@Getter
@Setter
@ToString
public class ResponseVO {

    private Date date;
    private YearMonth yearMonth;
    private LocalDate localDate;
    private LocalTime localTime;
    private LocalDateTime localDateTime;

}
