package site.hanzhe.example.jackson.datetime;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BootJacksonDatetime {

    public static void main(String[] args) {
        SpringApplication.run(BootJacksonDatetime.class, args);
    }

}