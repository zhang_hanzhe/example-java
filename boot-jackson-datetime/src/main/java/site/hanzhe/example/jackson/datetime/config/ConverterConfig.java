// package site.hanzhe.example.jackson.datetime.config;
//
// import cn.hutool.core.date.DatePattern;
// import org.springframework.context.annotation.Bean;
// import org.springframework.context.annotation.Configuration;
// import org.springframework.core.convert.converter.Converter;
//
// import java.time.LocalDate;
// import java.time.LocalDateTime;
// import java.time.LocalTime;
// import java.time.format.DateTimeFormatter;
//
// /**
//  * 数据类型全局转换器
//  *
//  * @author 张涵哲
//  * @since 2024-08-08 11:11:33
//  */
// @Configuration
// @SuppressWarnings("Convert2Lambda")
// public class ConverterConfig {
//
//     private final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern(DatePattern.NORM_DATE_PATTERN);
//     private final DateTimeFormatter TIME_FORMATTER = DateTimeFormatter.ofPattern(DatePattern.NORM_TIME_PATTERN);
//     private final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern(DatePattern.NORM_DATETIME_PATTERN);
//
//     @Bean
//     public Converter<String, LocalDate> strToLocalDateConverter() {
//         return new Converter<String, LocalDate>() {
//             @Override
//             public LocalDate convert(String source) {
//                 return LocalDate.parse(source, DATE_FORMATTER);
//             }
//         };
//         // 真奇怪，这里使用Lambda表达式会类型推断失败，抛出异常，必须使用匿名内部类
//         // return (String source) -> LocalDate.parse(source, DATE_FORMATTER);
//     }
//
//     @Bean
//     public Converter<String, LocalTime> strToLocalTimeConverter() {
//         return new Converter<String, LocalTime>() {
//             @Override
//             public LocalTime convert(String source) {
//                 return LocalTime.parse(source, TIME_FORMATTER);
//             }
//         };
//     }
//
//     @Bean
//     public Converter<String, LocalDateTime> strToLocalDateTimeConverter() {
//         return new Converter<String, LocalDateTime>() {
//             @Override
//             public LocalDateTime convert(String source) {
//                 return LocalDateTime.parse(source, DATE_TIME_FORMATTER);
//             }
//         };
//     }
//
// }
