package site.hanzhe.example.jackson.datetime.param;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.YearMonth;
import java.util.Date;

/**
 * ReuqestParam
 *
 * @author 张涵哲
 * @since 2024-08-13 11:33:26
 */
@Getter
@Setter
@ToString
public class RequestParam {

    private Date date;
    private YearMonth yearMonth;
    private LocalDate localDate;
    private LocalTime localTime;
    private LocalDateTime localDateTime;

}
