package site.hanzhe.example.boot3.captcha;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AjCaptchaApplication {

    public static void main(String[] args) {
        SpringApplication.run(AjCaptchaApplication.class, args);
    }

}
